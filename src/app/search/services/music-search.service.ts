import { Injectable, Inject } from "@angular/core";
import { API_URL } from "../API_URL";
import { Album } from "src/app/models/Album";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { AlbumsResponse } from "../../models/Album";
import { AuthService } from "../../security/auth.service";
import { Observable, EMPTY, NEVER, of, throwError } from "rxjs";
import { map, pluck, catchError } from "rxjs/operators";
// import { SearchModule } from '../search.module';

// console.log('service loaderd')

@Injectable({
  // providedIn: SearchModule
  providedIn: "root"
})
export class MusicSearchService {
  constructor(
    @Inject(API_URL)
    private api_url: string,
    private http: HttpClient,
    private auth: AuthService
  ) {}

  getAlbums(query = "batman") {
    return this.http
      .get<AlbumsResponse>(this.api_url, {
        headers: {
          Authorization: "Bearer " + this.auth.getToken()
        },
        params: {
          type: "album",
          q: query
        }
      })
      .pipe(
        map(resp => resp.albums.items),
        // pluck("albums", "items"),
        catchError(err => {
          if (err instanceof HttpErrorResponse) {
            if (err.status === 401) {
              this.auth.authorize();
            }
            return throwError(err.error.error);
          }
          return throwError(err);
          // return EMPTY
          // return of(this.results)
          // return NEVER
        })
      );
  }

  results: Album[] = [
    {
      id: "123",
      name: "Serviec album",
      artists: [],
      images: [
        {
          url: "https://www.placecage.com/c/200/200"
        }
      ]
    }
  ];
}
