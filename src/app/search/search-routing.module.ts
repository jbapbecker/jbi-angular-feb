import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { SearchViewComponent } from "./views/search-view/search-view.component";

const routes: Routes = [
  {
    path: "",
    component: SearchViewComponent
  },
  {
    path: "music",
    component: SearchViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SearchRoutingModule {}
