export interface Playlist {
  id: number;
  name: string;
  favourite: boolean;
  /**
   * HEX Color value
   */
  color: string;
  tracks?: Track[];
}

interface Track {}

// const p: Playlist = {
//   id: "123",
//   name: "",
//   favourite: true
// };

// interface Point {
//   x: number;
//   y: number;
// }

// interface Vector {
//   x: number;
//   y: number;
// }

// let p:Point = {x:1, y:2}

// let v:Vector = p
