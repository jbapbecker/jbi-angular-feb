import {
  Component,
  OnInit,
  ViewEncapsulation,
  Input,
  Output,
  EventEmitter
} from "@angular/core";
import { Playlist } from "src/app/models/Playlist";
import { NgIf, NgForOf, NgForOfContext } from "@angular/common";

NgForOfContext;

@Component({
  selector: "app-items-list",
  templateUrl: "./items-list.component.html",
  styleUrls: ["./items-list.component.scss"]
  // encapsulation: ViewEncapsulation.Emulated
})
export class ItemsListComponent implements OnInit {
  @Input("items")
  playlists: Playlist[] = [];

  @Output()
  selectedChange = new EventEmitter<Playlist>();

  @Input()
  selected?: Playlist;

  select(p: Playlist) {
    this.selectedChange.emit(this.selected == p ? undefined : p);
  }

  constructor() {}

  ngOnInit() {}
}
