import { Component, OnInit } from "@angular/core";
import { Playlist } from "src/app/models/Playlist";

enum MODE {
  show = "show",
  edit = "edit"
}
// console.log(MODE);

@Component({
  selector: "app-playlists-view",
  templateUrl: "./playlists-view.component.html",
  styleUrls: ["./playlists-view.component.scss"]
})
export class PlaylistsViewComponent {
  mode = "show";

  playlists: Playlist[] = [
    {
      id: 123,
      favourite: true,
      name: "Angular Hits",
      color: "#ff00ff"
    },
    {
      id: 234,
      favourite: false,
      name: "Angular TOP20",
      color: "#00ffff"
    },
    {
      id: 345,
      favourite: true,
      name: "Best of Angular",
      color: "#ffff00"
    }
  ];

  selected?: Playlist;

  constructor() {}

  ngOnInit() {}

  edit() {
    this.mode = "edit";
  }

  cancel() {
    this.mode = "show";
  }

  save(draft: Playlist) {
    // console.log(draft);
    this.selected = draft;
    this.playlists = this.playlists.map(p => {
      return p.id == draft.id ? draft : p;
    });
    this.mode = "show";
  }
}
