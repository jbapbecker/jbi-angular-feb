import { BrowserModule } from "@angular/platform-browser";
import { NgModule, ApplicationRef } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { PlaylistsModule } from "./playlists/playlists.module";
import { SecurityModule } from "./security/security.module";
import { API_URL } from './search/API_URL';
import { environment } from 'src/environments/environment';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [AppComponent],
  // entryComponents: [AppComponent],
  imports: [
    BrowserModule,
    PlaylistsModule,  
    HttpClientModule, 
    // SearchModule,
    SecurityModule,
    AppRoutingModule
  ],
  providers: [
    {
      provide: API_URL,
      useValue: environment.spotify_url
    }
  ],
  bootstrap: [AppComponent /* ,HeaderComponent, MenuComponent */]
})
export class AppModule {
  // constructor(private app: ApplicationRef) {}
  // ngDoBootstrap() {
  //   // donwload some config from server, and then...
  //   this.app.bootstrap(AppComponent, "app-root");
  // }
}
