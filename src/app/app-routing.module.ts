import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PlaylistsViewComponent } from "./playlists/views/playlists-view/playlists-view.component";

const routes: Routes = [
  {
    path: "",
    redirectTo: "playlists",
    pathMatch: "full"
  },
  {
    path: "playlists",
    component: PlaylistsViewComponent
    // resolve:[ PlalistsDataResolver ]
  },
  {
    path: "playlists/:playlist_id",
    component: PlaylistsViewComponent
    // resolve:[ PlalistsDataResolver ]
  },
  {
    path: "search",
    loadChildren: () =>
      import('./search/search.module').then(m => m.SearchModule)
  },
  {
    path: "**",
    // component: PageNotFoundViewComponent
    redirectTo: "playlists",
    pathMatch: "full"
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      enableTracing: true
      // useHash: true
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
